//
//  Pokemon.swift
//  PokedexJ
//
//  Created by Jossué Dután on 13/6/18.
//  Copyright © 2018 Jossué Dután. All rights reserved.
//

import Foundation

struct Pokemon:Decodable {
    var name:String
    var weight:Int
    var height:Int
    var sprites:Sprite
}

struct Sprite:Decodable {
    var defaultSprite: String
    
    //para que se venga lo de front_default del Json al defaultSprite
    enum CodingKeys: String, CodingKey{
        case defaultSprite = "front_default"
    }
}
