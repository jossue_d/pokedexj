//
//  PokemonViewController.swift
//  PokedexJ
//
//  Created by Jossué Dután on 20/6/18.
//  Copyright © 2018 Jossué Dután. All rights reserved.
//

import UIKit

class PokemonViewController: UIViewController {

    @IBOutlet weak var pokemonName: UILabel!
    
    @IBOutlet weak var pokemonWeight: UILabel!
    
    @IBOutlet weak var pokemonHeight: UILabel!
    
    @IBOutlet weak var pokemonImage: UIImageView!
    
    
    var pokemonInfo:(pokemon:Pokemon?, index: Int)?
    var pokemon:Pokemon!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pokemonName.text = self.pokemon.name
        self.pokemonHeight.text = String(self.pokemon.height)
        self.pokemonWeight.text = String(self.pokemon.weight)

        
        
        let bm = Network()
        
        bm.getImage((pokemon?.sprites.defaultSprite)!, completionHandler: { (imageR) in
            
            DispatchQueue.main.async {
                self.pokemonImage.image = imageR
            }
            
        })
        
        
            
        //pokemonImage.image = pokemon.imagen
            
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
