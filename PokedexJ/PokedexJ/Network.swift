//
//  Network.swift
//  PokedexJ
//
//  Created by Jossué Dután on 13/6/18.
//  Copyright © 2018 Jossué Dután. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

class Network {
    func getAllPokemon(completion:@escaping ([Pokemon])->()){
        var pokemonArray:[Pokemon] = []
        let group = DispatchGroup() //creamos el group
        for i in 1...8{
            group.enter() //inicio
            Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseJSON{ response in
                
                guard let data = response.data else{
                    print("Error")
                    return
                }
                
                guard let pokemon = try? JSONDecoder().decode(Pokemon.self, from: data) else {
                    print("Error decoding Pokemon")
                    return
                }
                
                print(pokemon)
                pokemonArray.append(pokemon)
                group.leave() //fin, el for se hace más rápido
            }
        }
        group.notify(queue: .main){ //main es el hilo principal
            completion(pokemonArray)
        }
        
    }
    func getPokemonImage(_ id:Int, completionHandler: @escaping(UIImage)->()){
        
        let url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png"
        
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                completionHandler(image)
            }
        }
    }
    func getImage (_ url:String, completionHandler: @escaping(UIImage)->()){
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                completionHandler(image)
            }
        }
    }
}
