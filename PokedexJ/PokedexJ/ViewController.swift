//
//  ViewController.swift
//  PokedexJ
//
//  Created by Jossué Dután on 13/6/18.
//  Copyright © 2018 Jossué Dután. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var pokemon:[Pokemon] = []
    
    @IBOutlet weak var pokemonTableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let network = Network()
        network.getAllPokemon{ (pokemonArray) in
            self.pokemon = pokemonArray
            self.pokemonTableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toPokemonInfoSegue"{
            let destination = segue.destination as! PokemonViewController
            let selectedRow = pokemonTableView.indexPathsForSelectedRows![0]
            //destination.pokemon = (self.pokemon, selectedRow.row)
            destination.pokemon = self.pokemon[selectedRow.row]
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toPokemonInfoSegue", sender: self)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemon.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = pokemon[indexPath.row].name
        return cell
    }


}

